Ejercicios:1.  
1. Al montarlo. ¿ Para qu ́e se ha puestoumask=000?
2.  Cargando elMBR 
a. Mostrando elMBRcon elHex Editor: Muestre los primeros bytes y la tabla de particiones. ¿Cu ́antas particioneshay ? Muestre claramente en qu ́e lugar puede observarlo.
b.  Lea los datos del punto anterior utilizando c ́odigoCy mu ́estrelos por pantalla.
c.  Muestre en elHex Editorsi la primer partici ́on es booteable o no. ¿Lo es?
d.  Muestre, mediante un programa enC, para la primer partici ́on: el flag de booteable, la direcci ́onCylinder-head-sector (chs), el tipo de partici ́on y su tama ̃no en sectores.
3.  Cargando  la  tabla  de  archivos.  En  todos  los  ejemplos  siguientes,  cuando  se  pida  c ́odigoC,  deber ́a  leer  la  tabla  departiciones e ir recorriendo las estructuras de datos adecuadamente. Es decir no podr ́a hardcodear direcciones vistasde alguna otra manera, salvo que se indique lo contrario.
a.  ¿Cu ́antos y cu ́ales archivos tiene el filesystem ? Mu ́esrelos con Bless y genere el c ́odigoCpara mostrarlos.
b.  Montando el filesystem (mediantemount) cree un archivo en la carpeta root/y luego b ́orrelo. B́usquelo por blessy mu ́estrelo en con el c ́odigo generado previamente.
c)  Muestre medante bless el archivo que ha sido borrado. Explique c ́omo lo ha visto. Genere c ́odigoCpara mostrarlos.
d)  ¿Que puede decir acerca del recupero de archivos ?

4.  Leyendo archivos.
a)  Montando el filesystem (mediantemount) cree un archivo llamado lapapa.txt y p ́ongale algun texto como con-tenido. Hagalo en la carpeta root/. . B ́usquelo por bless y mu ́estrelo en con el codigo generado previamente.
b)  Muestre, mediante elhex editory mediante c ́odigoClo que hay en el archivo no borrado.
c)  Cree codigoC para que dado un archivo (o una parte), lo busque y si lo encuentra y el mismo se encuentra borrado,lo recupere.

Referencias:https://en.wikipedia.org/wiki/DesignoftheFATfilesystem
http://www.c-jump.com/CIS24/Slides/FileSysDataStructs/FileSysDataStructs.html2